option('non_glibc',
	type: 'boolean',
	value: 'false',
	
	description: 'Enable non-GNU libc support'
)
