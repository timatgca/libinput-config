Name:     libinput-config
Version:  0.3
Release:  1%{?dist}
Summary:  packaging of Kirby Kevinson / libinput-config
License:  MIT
URL:      https://gitlab.com/timatgca/libinput-config
source0:  https://gitlab.com/timatgca/libinput-config

BuildArch: x86_64
BuildRequires: libinput-devel, libudev-devel, cmake
BuildRequires:  git-core
BuildRequires:  gcc
BuildRequires:  meson
BuildRequires:  pkgconfig(libudev)
BuildRequires:  pkgconfig(mtdev) >= 1.1.0
BuildRequires:  pkgconfig(libevdev) >= 0.4
BuildRequires:  pkgconfig(libwacom) >= 0.20
BuildRequires:  python3-devel
BuildRequires:  check-devel

Requires(post): info
Requires(preun): info

%description
libinput-config allows you to configure your inputs in case your
Wayland compositor doesn't have a certain config or has none.

%prep
%autosetup

%build
%meson build

%install
%cd build
%meson install

%post
/sbin/install-info %{_infodir}/%{name}.info %{_infodir}/dir || :

%preun
if [ $1 = 0 ] ; then
/sbin/install-info --delete %{_infodir}/%{name}.info %{_infodir}/dir || :
fi

%files -f %{name}.lang
%{_mandir}/man1/hello.1.*
%{_infodir}/hello.info.*
%{_bindir}/hello

%doc AUTHORS ChangeLog NEWS README THANKS TODO
%license COPYING

%changelog
